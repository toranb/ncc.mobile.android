package com.nebraska.entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class SessionDeserializer {
    public List<Session> GetSessionsFromJson(String json) {
        ArrayList<Session> sessions = new ArrayList<Session>();

        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONObject a = (JSONObject) jsonObj.get("d");
            JSONArray data = (JSONArray) a.get("data");

            for (int i = 0; i < data.length(); i++) {
                Session session = new Session();

                JSONObject item = (JSONObject) data.get(i);

                session.setTime(String.valueOf(item.get("Time")).trim());
                session.setSession(String.valueOf(item.get("Session")).trim());
                session.setDesc(String.valueOf(item.get("Desc")).trim());
                session.setRoom(String.valueOf(item.get("Room")));

                JSONObject speakerJsonObj = (JSONObject) item.get("Speaker");
                Speaker speaker = new Speaker();
                speaker.setBio(speakerJsonObj.getString("Bio"));
                speaker.setName(speakerJsonObj.getString("Name"));
                speaker.setImg(speakerJsonObj.getString("Img"));
                speaker.setLocation(speakerJsonObj.getString("Location"));
                speaker.setWeb(speakerJsonObj.getString("Web"));

                session.setSpeaker(speaker);

                sessions.add(session);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sessions;
    }
}
