package com.nebraska.entity;

public class SpecialSessionIdentifier {
    public boolean sessionNameRequiresSpecialTreatment(String sessionName) {
        if (sessionName.toLowerCase().equals("registration and breakfast"))
            return true;

        if (sessionName.toLowerCase().equals("welcome and announcements"))
            return true;

        if (sessionName.toLowerCase().equals("break"))
            return true;

        if (sessionName.toLowerCase().equals("lunch"))
            return true;

        if (sessionName.toLowerCase().equals("closing and prizes"))
            return true;

        if (sessionName.toLowerCase().equals("after party"))
            return true;

        return false;
    }
}
